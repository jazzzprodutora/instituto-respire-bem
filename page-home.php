<?php // Template Name: Home ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <!-- MODALS -->
    <?php require 'templates/modals.php' ?>

    <!-- HEADER -->
    <section class="header" id="principal">
        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- BANNER -->
        <div class="banner wow fadeIn" data-wow-duration="2s">
            <div class="container">
                <div class="chamada">
                    <div class="info wow fadeInDown" data-wow-duration="3s">
                        <h1 class="titulo">Somos uma clínica especializada em pneumologia</h1>
                        <div class="area-botao">
                            <a href="#especialidades" class="botao botao-principal scroll">Saiba mais <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/seta-branca-menor.png"></a>
                        </div>
                    </div>
                    <div class="formulario wow fadeInRight" data-wow-duration="2s">
                        <h2 class="titulo">Faça sua consulta</h2>
                        <p class="texto">Entre em contato! Agende sua consulta com nossos especialistas preenchendo o formulário abaixo. Nós retornamos o mais breve possível.</p>
                        <form action="">
                            <input type="text" placeholder="nome">
                            <input type="tel" placeholder="telefone">
                            <input type="email" placeholder="e-mail">
                            <textarea cols="30" rows="10" placeholder="mensagem"></textarea>
                        </form>
                        <div class="area-botao">
                            <a href="" class="botao botao-principal">Entrar em contato</a>
                        </div>
                        <div class="vamos-conversar">
                            <a href="https://api.whatsapp.com/send?phone=8796565595&text=Ol%C3%A1,%20gostaria%20de%20agendar%20uma%20consulta" target="_blank">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp-branco.png" alt="Whatsapp">
                                <p>Agende consulta pelo<span>whatsapp</span></p>
                            </a>
                        </div>
                    </div>
                </div>
                <a href="#sobre" class="scroll">
                    <div class="seta-nav wow fadeInUp" data-wow-duration="2s">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/seta-branca.png">
                    </div>
                </a>
            </div>
        </div>
    </section>

    <!-- AMBIENTE -->
    <section class="ambiente" id="sobre">
        <div class="container-fluid">
            <div class="itens">
                <div class="item wow fadeIn" data-wow-duration="3s">
                    <h2 class="titulo">Um ambiente que garante seu atendimento com toda a segurança e qualidade, e o melhor: pertinho de você!</h2>
                    <p class="texto">Bem-vindo ao Instituto Respire Bem!<br><br>
                    Somos a primeira e única clínica especializada em Pneumologia com atendimento para Clínica geral do Sertão Central de Pernambuco, no Município de Cedro-PE. Aqui você enconrta uma estrutura ampla e bem equipada para realizar os exames que vão auxiliar no seu diagnóstico.  <br><br>
                    São profissionais capacitados e preparados para te oferecer um atendimento de qualidade, com o conforto e a comodidade que você merece. 

                    Nossos profissionais são capacitados e preparados para te oferecer um atendimento de qualidade, com o conforto e a comodidade que você merece. Nosso objetivo é cuidar de cada paciente de forma única, atendendo às suas necessidades, sem que seja necessário se deslocar do interior para a capital. É mais qualidade de vida e saúde acessível para você!</p>
                </div>
                <!-- CAROUSEL -->
                <div id="carouselAmbiente" class="carousel slide wow fadeInRight" data-wow-duration="3s" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/fachada2.jpeg">
                        </div>
                        <div class="carousel-item">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/atendimento1.jpg">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselAmbiente" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselAmbiente" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Proximo</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- ESTRUTURA -->
    <section class="estrutura">
        <div class="container-fluid">
            <!-- CAROUSEL -->
            <div id="carouselEstrutura" class="carousel slide" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#carouselEstrutura" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselEstrutura" data-slide-to="1"></li>
                    <li data-target="#carouselEstrutura" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="faixa">
                            <div class="titulo-faixa">
                                <h4 class="titulo">Estrutura</h4>
                            </div>
                            <!-- <div class="texto-faixa">
                                <p class="texto">/ sala privativa</p>
                            </div> -->
                        </div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/atendimento2.jpg">
                    </div>
                    <div class="carousel-item">
                        <div class="faixa">
                            <div class="titulo-faixa">
                                <h4 class="titulo">Estrutura</h4>
                            </div>
                            <!-- <div class="texto-faixa">
                                <p class="texto">/ sala privativa</p>
                            </div> -->
                        </div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/equipe2.jpg">
                    </div>
                    <div class="carousel-item">
                        <div class="faixa">
                            <div class="titulo-faixa">
                                <h4 class="titulo">Estrutura</h4>
                            </div>
                            <!-- <div class="texto-faixa">
                                <p class="texto">/ sala privativa</p>
                            </div> -->
                        </div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/entrada1.jpg">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselEstrutura" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a class="carousel-control-next" href="#carouselEstrutura" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Proximo</span>
                </a>
            </div>
        </div>
    </section>

    <!-- SERVICOS -->
    <section class="servicos" id="consultas">
        <div class="container">
            <h1 class="titulo wow fadeInDown" data-wow-duration="2s">Nossos Serviços</h1>
            <div class="itens">
                <div class="item wow fadeIn" data-wow-duration="3s">
                    <div class="info">
                        <h2 class="titulo-item">Consultas para Clínico Geral</h2>
                        <p class="texto-corrido">Faça seus exames de rotina perto de casa, sem a necessidade de se deslocar para a capital. Temos a sua disposição especialistas em Clínica geral que proporcionam atendimento humanizado para todos os pacientes.</p>
                        <!-- <p class="texto">A partir de R$ ---,00 <span>/ pessoa</span></p>
                        <p class="texto-alt">A partir de 02 pessoas R$ ---,00 cada</p> -->
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/servico1.jpg">
                </div>
                <div class="item wow fadeIn" data-wow-duration="2s" data-wow-delay=".8s">
                    <div class="info">
                        <h2 class="titulo-item">Consultas para Pneumologia</h2>
                        <p class="texto-corrido">A especialidade da nossa clínica é a Pneumologia. E aqui nós temos profissionais qualificados e especializados que farão um diagnóstico, tratamento e controle de doenças próximo a você. </p>
                        <!-- <p class="texto">A partir de R$ ---,00 <span>/ mes</span></p> -->
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/servico2.jpg">
                </div>
                <div class="item wow fadeIn" data-wow-duration="2s" data-wow-delay=".8s">
                    <div class="info">
                        <h2 class="titulo-item">Exames</h2>
                        <p class="texto-corrido">Faça seus exames com qualidade e agilidade sem precisa se deslocar para longe da sua casa. Temos:</p>
                        <div class="linha">
                            <p class="texto-corrido link-modal" data-toggle="modal" data-target="#examesLaboratoriais">Exames Laboratoriais <span>+</span></p>
                            <p class="texto-corrido">Espirometria</p>
                            <p class="texto-corrido">Eletrocardiograma</p>
                        </div>
                        <!-- <p class="texto">A partir de R$ ---,00 <span>/ mes</span></p> -->
                    </div>
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/servico3.jpg">
                </div>
            </div>
            <div class="linha">
                <div class="area-botao wow fadeInUp" data-wow-duration="2s">
                    <a href="#contatos" class="botao botao-principal scroll">Entre em contato</a>
                </div>
                <div class="vamos-conversar wow fadeInUp" data-wow-duration="3s">
                    <a href="https://api.whatsapp.com/send?phone=8796565595&text=Ol%C3%A1,%20gostaria%20de%20agendar%20uma%20consulta" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp-verde.png" alt="Whatsapp">
                        <p>Agende pelo whatsapp<span>87 99656 5595</span></p>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- ESPECIALIDADES E DIFERENCIAIS -->
    <section class="especialidade-diferenciais" id="especialidades">
        <div class="container">
            <h1 class="titulo wow fadeInDown" data-wow-duration="2s">Especialidades e Diferenciais</h1>
            <div class="itens wow fadeIn" data-wow-duration="4s">
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/1.png">
                    <p class="texto">Única clínica especializada em <span>Pneumologia</span> no interior de PE</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/2.png">
                    <p class="texto">Realizamos também atendimento <span>Clínico Geral</span></p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/3.png">
                    <p class="texto">Consultas, tratamentos e exames</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/4.png">
                    <p class="texto">Os melhores especialistas do sertão central de PE</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/5.png">
                    <p class="texto">Qualidade e eficiência nos resultados</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/6.png">
                    <p class="texto">Localizado no entorno das principais cidades do interior / PE</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/7.png">
                    <p class="texto">Agendamento de consultas por telefone e whatsapp</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/8.png">
                    <p class="texto">Atendimento de segunda a sábado<br>(horário comercial)</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/9.png">
                    <p class="texto">Envio de exames digitalizados</p>
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/diferenciais/10.png">
                    <p class="texto">Ambiente todo higienizado</p>
                </div>
            </div>
            <div class="linha">
                <div class="area-botao wow fadeInUp" data-wow-duration="2s">
                    <a href="#contatos" class="botao botao-principal scroll">Entre em contato</a>
                </div>
                <div class="vamos-conversar wow fadeInUp" data-wow-duration="3s">
                    <a href="https://api.whatsapp.com/send?phone=8796565595&text=Ol%C3%A1,%20gostaria%20de%20agendar%20uma%20consulta" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp-verde.png" alt="Whatsapp">
                        <p>Agende pelo whatsapp<span>87 99656 5595</span></p>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- LOCALIZACAO -->
    <section class="localizacao wow fadeIn" data-wow-duration="3s">
        <div class="container">
            <p class="texto">Agora você não precisa mais se deslocar do Sertão para a capital!</p>
            <p class="texto"><span>Nós temos uma</span><br>localização estratégica<br><span> para economizar seu tempo de deslocamento.</span></p>
        </div>
    </section>

    <!-- CONTATO -->
    <section class="contato" id="contatos">
        <div class="container">
            <h1 class="titulo wow fadeInDown" data-wow-duration="2s">Contato</h1>
            <p class="texto">Entre em contato, agende uma consulta e em breve retornaremos com maiores informações.<br>Obrigado!</p>
            <div class="linha">
                <div class="formulario wow fadeIn" data-wow-duration="2s">
                    <form action="">
                        <div class="campo">
                            <span>Nome</span>
                            <input type="text">
                        </div>
                        <div class="campo">
                            <span>Email</span>
                            <input type="email">
                        </div>
                        <div class="campo">
                            <span>Telefone</span>
                            <input type="fone">
                        </div>
                        <div class="campo">
                            <span>Mensagem</span>
                            <textarea cols="30" rows="10"></textarea>
                        </div>
                    </form>
                    <div class="area-botao">
                        <a href="" class="botao botao-principal">Enviar</a>
                    </div>
                </div>
                <div class="info-contato wow fadeIn" data-wow-duration="2s">
                    <div class="item">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp-roxo.png">
                        <div class="info">
                            <h4 class="titulo">Vamos conversar?</h4>
                            <a href="https://api.whatsapp.com/send?phone=8796565595&text=Ol%C3%A1,%20gostaria%20de%20agendar%20uma%20consulta" target="_blank">87 9656 5595</a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/local.png">
                        <div class="info">
                            <p class="texto">Rua Nossa do Perpétuo Socorro, 152<br>
                                Centro, Cedro/PE</p>
                            <a href="https://www.google.com.br/maps/place/R.+Nossa+Sra.+Perp%C3%A9tuo+Socorro,+152,+Cedro+-+PE,+56130-000/@-7.7203526,-39.2415064,17z/data=!3m1!4b1!4m5!3m4!1s0x7a0fe75d9c6cb1d:0x8f3b508edc15dc34!8m2!3d-7.7203526!4d-39.2393177" target="_blank">Ver Mapa</a>
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mail-roxo.png">
                        <div class="info">
                            <a href="mailto:pneumosertao@gmail.com">pneumosertao@gmail.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<!-- FOOTER -->
<?php require 'footer.php' ?>