	<footer class="rodape">
		<!-- ASSINATURA -->
		<div class="assinatura">
			<div class="container">
				<div class="itens">
					<div class="item">
						<p>2020 IRB - Instituto Respire Bem</p>
					</div>
					<div class="item">
						<a href="http://jazzz.com.br/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/logo-jazzz.png" alt="Jazzz Agência Digital"></a>
					</div>
				</div>
			</div>
		</div>
	</footer>


	<!-- CHAMA O JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- CHAMA O JS DO SCROLL -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll-suave.js"></script>

	<!-- CHAMA E INICIA O WOW.JS -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>

	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/menu-mobile.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>

	<?php wp_footer(); ?>

	
</body>
</html>
