<nav class="cabecalho">
    <div class="container">
        <div class="linha">
            <div class="logo">
                <a href="#principal" class="scroll"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/logo.png" alt="Instituto Respire Bem" class="logo-img"></a>
            </div>
            <div class="links">
                <a href="#principal" class="link scroll">Principal</a>
                <a href="#sobre" class="link scroll">Sobre</a>
                <a href="#especialidades" class="link scroll">Especialidades</a>
                <a href="#consultas" class="link scroll">Consultas</a>
                <!-- <a href="#blog" class="link scroll">Blog</a> -->
                <a href="#contatos" class="link scroll">Contatos</a>
            </div>
            <div class="redes-sociais">
                <a href="https://www.facebook.com/Instituto-Respire-Bem-110430084095750" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/facebook-roxo.png" alt="Facebook"></a>
                <a href="https://www.instagram.com/institutorespirebem/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram-roxo.png" alt="Instagram"></a>
            </div>
            <div class="textura">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/textura.png">
            </div>
            <div class="vamos-conversar">
                <a href="https://api.whatsapp.com/send?phone=8796565595&text=Ol%C3%A1,%20gostaria%20de%20agendar%20uma%20consulta" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp-verde.png" alt="Whatsapp">
                    <p>Vamos conversar? <span>87 9656-5595</span></p>
                </a>
            </div>
            <div class="menu-mobile">
                <div class="linha1"></div>
                <div class="linha2"></div>
                <div class="linha3"></div>
                <div class="menu-mobile-layer">
                    <div class="links-mobile">
                        <a href="#principal" class="link-mobile">Principal</a>
                        <a href="#sobre" class="link-mobile">Sobre</a>
                        <a href="#especialidades" class="link-mobile">Especialidades</a>
                        <a href="#consultas" class="link-mobile">Consultas</a>
                        <!-- <a href="#blog" class="link-mobile">Blog</a> -->
                        <a href="#contatos" class="link-mobile">Contatos</a>
                    </div>
                    <div class="redes-sociais">
                        <a href="https://www.facebook.com/Instituto-Respire-Bem-110430084095750" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/facebook-roxo.png" alt="Facebook"></a>
                        <a href="https://www.instagram.com/institutorespirebem/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram-roxo.png" alt="Instagram"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>