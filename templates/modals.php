<!-- MODAL EXAMES LABORATORIAIS -->
<div class="modal fade" id="examesLaboratoriais" tabindex="-1" aria-labelledby="examesLaboratoriaisLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="examesLaboratoriaisLabel">Exames Laboratoriais</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="botao botao-principal" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>