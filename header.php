<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <!-- SEO -->
    <title><?php bloginfo('name'); ?> - <?php the_title()?></title>
    <meta name="author" content="Jazzz - Hugo Queiroz">
    <meta name="description" content="">
    <meta name="keywords" content=""/>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/icons/favicon.png">
    <meta name="theme-color" content="#fff">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/estilo.css">


    <!-- WP -->
    <?php wp_head(); ?>
</head>
<body>