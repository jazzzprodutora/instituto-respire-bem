document.addEventListener('scroll', () => {
  var cabecalho = document.querySelector('.cabecalho');
  var logo = document.querySelector('.logo-img');
  if ($(document).scrollTop() > 100) {
    cabecalho.style.padding = '.5rem 0rem';
    logo.style.width = '80%';
  } else {
    cabecalho.style.padding = '1rem 0rem';
    logo.style.width = '90%';
  }
});
