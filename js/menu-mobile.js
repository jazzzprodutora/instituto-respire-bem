var menu = document.querySelector('.menu-mobile');

// ANIMACAO MENU
menu.addEventListener('click', () => {
  var linha1 = document.querySelector('.linha1');
  var linha2 = document.querySelector('.linha2');
  var linha3 = document.querySelector('.linha3');
  linha1.classList.toggle('linha1-efct');
  linha2.classList.toggle('linha2-efct');
  linha3.classList.toggle('linha3-efct');
});

// LAYER MENU MOBILE
menu.addEventListener('click', () => {
  var layerMenu = document.querySelector('.menu-mobile-layer');
  layerMenu.classList.toggle('menu-mobile-layer-efct');
});
